<?php
/**
 * Plugin Name: WPP Custom Settings
 * Plugin URI: http://oss.ruwe-digital.de
 * Version: 0.0.3
 * Author: Sebastian Ruwe
 * Author URI: http://oss.ruwe-digital.de
 * Description: WordPress Hardening and Settings Plugin
 * License: GNU
 */

/**
 * DEFINE Wordpress Settings
 *
 * 01	Define Debug Mode is enabled or disabled to Display PHP Errors
 * 	01.1	Define to load uncompressed CSS- and JavaScript
 * 	01.2	Define to output DB Queries
 * 02	Define Wordpress Auto-Updates
 * 03	Define Post Revisions is enabled or disabled
 * 	03.1	Define Limit of Post Revisions
 * 04	Define Autosave Interval | Value = Seconds | std. = 60
 * 05	Define Empty Trash Days | Value = Days | 0 = Disable Trashcan | std. = 30
 * 06	Define Editor is enabled or disabled (Theme and Plugin)
 * 
 */

	/** Define Debug Mode is enabled or disabled to Display PHP Errors */
		define('WP_DEBUG', false);
		define('WP_DEBUG_LOG', false);
		define('WP_DEBUG_DISPLAY', false);
		
		/** Define to load uncompressed CSS- and JavaScript */
			define('SCRIPT_DEBUG', false);
		
		/** Define to output DB Queries */
			define('SAVEQUERIES', false);
		
			/*
			 * Insert Code below e.g. in Footer if SAVEQUERIES = true to show output
			 *
				 if(curent_user_can('administrator'))){
				 global $wpdb;
				 echo'<pre>'.print_r($wpdb->queries,true).'</pre>';
				 }
			 */
	
	/** Define Wordpress Auto-Updates */
		define('WP_AUTO_UPDATE_CORE', true);
		
	/** Define Post Revisions is enabled or disabled */
		define('WP_POST_REVISIONS', true);
			
		/** Define Limit of Post Revisions */
			if (!defined('WP_POST_REVISIONS')){
				define('WP_POST_REVISIONS', 10);
			}
			
	/** Define Autosave Interval | Value = Seconds | std. = 60 */
		define('AUTOSAVE_INTERVAL',60);
		
	/** Define Empty Trash Days | Value = Days | 0 = Disable Trashcan | std. = 30 */
		define('EMPTY_TRASH_DAYS',30);
		
	/** Define Editor is enabled or disabled (Theme and Plugin) */
		define('DISALLOW_FILE_EDIT',true);

		
/**
 * HARDENING Wordpress
 * 
 * 01	Disable XMLRPC
 * 	01.1	Remove XMLRPC-Entry from HTTP-Header
 *
 */
		
	/** Disable XMLRPC */
		add_filter('xmlrpc_enabled','__return_false');
		
		/** Remove XMLRPC-Entry from HTTP-Header */
		function AH_remove_x_pingback($headers){
			unset($headers['X-Pingback']);
			return $headers;
		}
		add_filter('wp_headers','AH_remove_x_pingback');

		
/**
 * REMOVE Unnecessary Things
 *
 * 01	Remove Unnecessary Junk in the Header
 * 02	Remove RSS-Feeds
 * 03	Remove Header Links
 * 04	Remove Version Info from Head and Feeds
 *
 */

	/** Remove Unnecessary Junk in the Header */
		/** emoji */
		function disable_emoji_dequeue_script() {
			wp_dequeue_script( 'emoji' );
		}
		add_action( 'wp_print_scripts', 'disable_emoji_dequeue_script', 100 );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		
	/** Remove RSS-Feeds */
		function ah_disable_feed() {
			wp_die( __('Kein Feed verfuegbar. Bitte besuchen Sie unsere <a href="'. get_bloginfo('url') .'">Startseite</a>!') );
		}
		add_action('do_feed','ah_disable_feed',1);
		add_action('do_feed_rdf','ah_disable_feed',1);
		add_action('do_feed_rss','ah_disable_feed',1);
		add_action('do_feed_rss2','ah_disable_feed',1);
		add_action('do_feed_atom','ah_disable_feed',1);
		
	/** Remove Header Links */
		function remheadlink()
		{
			remove_action('wp_head','rsd_link');
			remove_action('wp_head','wp_generator');
			remove_action('wp_head','index_rel_link');
			remove_action('wp_head','wlwmanifest_link');
			remove_action('wp_head','feed_links',2);
			remove_action('wp_head','feed_links_extra',3);
			remove_action('wp_head','parent_post_rel_link',10,0);
			remove_action('wp_head','start_post_rel_link',10,0);
			remove_action('wp_head','wp_shortlink_wp_head',10,0);
			remove_action('wp_head','wp_shortlink_header',10,0);
			remove_action('wp_head','adjacent_posts_rel_link_wp_head',10,0);
		}
		add_action('init','remheadlink');
		
	/** Remove Version Info from Head and Feeds */
		function complete_version_removal() {
			return '';
		}
		add_filter('the_generator', 'complete_version_removal');

		
/**
 * SPEED UP
 *
 * 01	Add the google CDN version of jQuery for the frontend | Make sure you use this with wp_enqueue_script('jquery'); in header
 *
 */
		
	/** Add the google CDN version of jQuery for the frontend | Make sure you use this with wp_enqueue_script('jquery'); in header */
		function wpfme_jquery_enqueue() {
			wp_deregister_script('jquery');
			wp_register_script('jquery', "http".($_SERVER['SERVER_PORT'] == 443?"s":"")."://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js",false,null);
			wp_enqueue_script('jquery');
		}
		if (!is_admin()) add_action("wp_enqueue_scripts","wpfme_jquery_enqueue",11);

		
/**
 * CUSTOMIZE Dashboard-Widgets, -Menu and Admin-Bar
 *
 * 01	Remove Dashboard-Widgets
 * 02	Hide Unused Dashboard Menu Items
 * 03	Hide Admin-Bar Menu Items
 *
 */
		
	/** Remove Dashboard-Widgets */
		function ah_remove_dashboard_widgets() {
			global $wp_meta_boxes;
		
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
			//unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
			//unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		}
		add_action('wp_dashboard_setup','ah_remove_dashboard_widgets');

	/** Hide Unused Dashboard Menu Items */
		function emersonthis_custom_menu_page_removing() {
			//if(is_user_logged_in() && !current_user_can('administrator')){
			// remove_menu_page( 'index.php' );					//Dashboard
			remove_menu_page( 'jetpack' );						//Jetpack*
			//remove_menu_page( 'edit.php' );					//Posts
			//remove_menu_page( 'upload.php' );					//Media
			// remove_menu_page( 'edit.php?post_type=page' );	//Pages
			remove_menu_page( 'edit-comments.php' );			//Comments
			// remove_menu_page( 'themes.php' );				//Appearance
			// remove_menu_page( 'plugins.php' );				//Plugins
			// remove_menu_page( 'users.php' );					//Users
			// remove_menu_page( 'tools.php' );					//Tools
			// remove_menu_page( 'options-general.php' );		//Settings
			//}
		}
		add_action( 'admin_menu', 'emersonthis_custom_menu_page_removing' );
		
	/** Hide Admin-Bar Menu Items */
		function emersonthis_custom_menu_link_removing() {
			global $wp_admin_bar;
			//if (is_user_logged_in() && !current_user_can('administrator')) {
			//$wp_admin_bar->remove_menu('wp-logo');		//WordPress-Logo
			$wp_admin_bar->remove_menu('about');			//"About WordPress"-Link
			$wp_admin_bar->remove_menu('wporg');			//"WordPress.org"-Link
			$wp_admin_bar->remove_menu('documentation');	//"Documentation"-Link
			$wp_admin_bar->remove_menu('support-forums');	//"Support-Foren"-Link
			$wp_admin_bar->remove_menu('feedback');			//"Feedback"-Link
			//$wp_admin_bar->remove_menu('site-name');		//"{Sitename}"-Link
			//$wp_admin_bar->remove_menu('view-site');		//"Go to Webseite"-Link
			//$wp_admin_bar->remove_menu('updates');		//"Update"-Link
			$wp_admin_bar->remove_menu('comments');			//"Comment"-Link
			//$wp_admin_bar->remove_menu('new-content');	//"+ New"-Link
			//$wp_admin_bar->remove_menu('my-account');		//"User-Menue"-Link
			//}
		}
		add_action('wp_before_admin_bar_render','emersonthis_custom_menu_link_removing');

/**
 * CUSTOMIZE Backend
 * 
 * 01	Change Loginpage Image
 * 	01.1	Change Loginpage Image URL
 * 	01.2	Namespace Loginpage Image Headertitle
 * 02	Add Admin Headerimage in Dashboard
 * 03	Add Custom Dashboard Widget
 * 04	Userprofil Contactfields
 * 	04.1	Remove unnecessary Contactfields in Userprofil
 * 	04.2	Add Contactfields in Userprofil (e.g. <?php echo $curauth->twitter; ?>)
 * 05	Add Custom CSS for the Login Page (Create wp-login.css in your theme folder)
 * 06	Editor
 * 	06.1	Add Automatic Media Shortcodes
 * 	06.2	Add Custom CSS to backend Editor 
 * 07	Add Post/Page IDs in Admin Table
 * 08	Capture users last login date and time
 * 	08.1	Set last login date
 * 	08.2	Get last login date
 * 09	Add New Role
 * 	09.1	Remove Role
 * 10	Allow SVG through WordPress Media Uploader
 *
 */
		
	/** Change Loginpage Image */
		add_action('login_head','namespace_login_style');
		function namespace_login_style() {
			echo '<style>.login h1 a { background-image: url(';
			echo get_template_directory_uri().'/images/logo.png';
			echo ') !important; }</style>';
		}
		
		/** Change Loginpage Image URL */
			function namespace_login_headerurl($url) {
				$url = home_url('/');
				return $url;
			}
			add_filter('login_headerurl','namespace_login_headerurl');
		
		/** Change Namespace Loginpage Image Headertitle */
			function namespace_login_headertitle($title) {
				$title = get_bloginfo('name');
				return $title;
			}
			add_filter('login_headertitle','namespace_login_headertitle');

	/** Add Admin Headerimage in Dashboard */
		function ah_custom_logo() {
			echo '<style type="text/css">
				#header-logo { background-image: url('.get_bloginfo('template_directory').'/images/custom-logo.png) !important; }
			</style>';
		}
		add_action('admin_head', 'ah_custom_logo');
		
	/** Add Custom Dashboard Widget */
		function ah_custom_dashboard_widgets() {
			global $wp_meta_boxes;
			wp_add_dashboard_widget('custom_help_widget','Theme Support','ah_custom_dashboard_help');
		}
		function ah_custom_dashboard_help() {
			echo '<p>Willkommen, du brauchst Hilfe? Dann kontaktiere den Entwickler</p>';
		}
		add_action('wp_dashboard_setup','ah_custom_dashboard_widgets');
		
	/** Userprofil Contactfields */
		/** Remove unnecessary Contactfields in Userprofil */
			function ah_hide_profile_fields($contactmethods) {
				unset($contactmethods['aim']);
				unset($contactmethods['jabber']);
				unset($contactmethods['yim']);
				return $contactmethods;
			}
			add_filter('user_contactmethods','ah_hide_profile_fields',10,1);
			
		/** Add Contactfields in Userprofil (e.g. <?php echo $curauth->twitter; ?>) */
			function ah_new_contactmethods($contactmethods) {
				// Twitter
				$contactmethods['twitter'] = 'Twitter';
				// Facebook
				$contactmethods['facebook'] = 'Facebook';
				return $contactmethods;
			}
			add_filter('user_contactmethods','ah_new_contactmethods',10,1);
		
	/** Add Custom CSS for the Login Page (Create wp-login.css in your theme folder) */
		function wpfme_loginCSS() {
			echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/wp-login.css"/>';
		}
		add_action('login_head', 'wpfme_loginCSS');
	
	/** Editor */
		/** Add Automatic Media Shortcodes */
			function add_sc_select(){
				global $shortcode_tags;
					
				/** Enter names of shortcode to exclude bellow */
				$exclude = array("wp_caption","embed",preg_grep("/^su*/"));
					
				echo '&nbsp;<select id="sc_select"><option>Shortcode</option>';
				foreach ($shortcode_tags as $key => $val){
					if(!in_array($key,$exclude)){
						$shortcodes_list .= '<option value="['.$key.'][/'.$key.']">'.$key.'</option>';
					}
				}
				echo $shortcodes_list;
				echo '</select>';
			}
			add_action('media_buttons','add_sc_select',11);
			
			function button_js() {
				echo '<script type="text/javascript">
				        jQuery(document).ready(function(){
				           jQuery("#sc_select").change(function() {
				                          send_to_editor(jQuery("#sc_select :selected").val());
				                          return false;
				                });
				        });
				        </script>';
			}
			add_action('admin_head', 'button_js');
			
		/** Add Custom CSS to backend Editor */			
			function add_theme_editor_styles() {
				add_editor_style( get_stylesheet_uri() );
			}
			add_action( 'init','add_theme_editor_styles');
		
	/** Add Post/Page IDs in Admin Table */		
		function posts_columns_id($defaults){
			$defaults['wps_post_id'] = __('ID');
			return $defaults;
		}
		
		function posts_custom_id_columns($column_name, $id){
			if($column_name === 'wps_post_id'){
				echo $id;
			}
		}
		add_filter('manage_posts_columns', 'posts_columns_id', 5);
		add_action('manage_posts_custom_column', 'posts_custom_id_columns', 5, 2);
		add_filter('manage_pages_columns', 'posts_columns_id', 5);
		add_action('manage_pages_custom_column', 'posts_custom_id_columns', 5, 2);
	
	/** Capture users last login date and time */
		/** Set last login date */
			add_action('wp_login','wpsnipp_set_last_login', 0, 2);
			function wpsnipp_set_last_login($login, $user) {
				$user = get_user_by('login',$login);
				$time = current_time( 'timestamp' );
				$last_login = get_user_meta( $user->ID, '_last_login', 'true' );
				if(!$last_login){
					update_usermeta( $user->ID, '_last_login', $time );
				}else{
					update_usermeta( $user->ID, '_last_login_prev', $last_login );
					update_usermeta( $user->ID, '_last_login', $time );
				}
			}
			
		/** Get last login date */
			function wpsnipp_get_last_login($user_id,$prev=null){
				$last_login = get_user_meta($user_id);
				$time = current_time( 'timestamp' );
				if(isset($last_login['_last_login_prev'][0]) && $prev){
					$last_login = get_user_meta($user_id, '_last_login_prev', 'true' );
				}else if(isset($last_login['_last_login'][0])){
					$last_login = get_user_meta($user_id, '_last_login', 'true' );
				}else{
					update_usermeta( $user_id, '_last_login', $time );
					$last_login = $last_login['_last_login'][0];
				}
				return $last_login;
			}	
	
	/**
	 * Add New Role
	 * e.g. To add the new role, using 'international' as the short name
	 * and 'International Blogger' as the displayed name in the User list and edit page:
	 */
		/*
		 add_role('international', 'International Blogger', array(
		 'read' => true, // True allows that capability, False specifically removes it.
		 'edit_posts' => true,
		 'delete_posts' => true,
		 'edit_published_posts' => true,
		 'publish_posts' => true,
		 'edit_files' => true,
		 'import' => true,
		 'upload_files' => true
		 ));
		 */
		
		/** Remove Role | To remove one outright or remove one of the defaults: */
		// remove_role('international');
		
	/** Allow SVG through WordPress Media Uploader */	
		function cc_mime_types($mimes) {
			$mimes['svg'] = 'image/svg+xml';
			return $mimes;
		}
		add_filter('upload_mimes', 'cc_mime_types');

/**
 * CUSTOMIZE Frontend
 * 
 * 01	Add Custom Post Types for WordPress-Search
 * 02	Add Custom excerpt length
 * 03	Add Favicon
 * 04	Remove Categories from Frontpage
 * 05	Remove Width and Height Attributes From Inserted Images
 * 06	Deregister Contact Form 7 CSS
 * 
 */
	/** Add Custom Post Types for WordPress-Search */
		function ah_searchAll( $query ) {
			if ( $query->is_search ) { $query->set( 'post_type', array( 'site','plugin', 'theme','person' )); } // Custom Post Types noch anpassen
			return $query;
		}
		add_filter( 'the_search_query', 'ah_searchAll' );
		
	/** Add Custom excerpt length */
		function wpfme_custom_excerpt_length( $length ) {
			//the amount of words to return
			return 150;
		}
		add_filter( 'excerpt_length', 'wpfme_custom_excerpt_length');
		
	/** Add Favicon */
		function ah_blog_favicon() {
			echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('wpurl').'/favicon.ico" />';
		}
		add_action('wp_head','ah_blog_favicon');
		
	/** Remove Categories from Frontpage */
		function exclude_category_home($query) {
			if($query->is_home) {
				$query->set('cat','-4,-34');
			}
			return $query;
		}
		add_filter('pre_get_posts','exclude_category_home');
		
	/** Remove Width and Height Attributes From Inserted Images */		
		function remove_width_attribute( $html ) {
			$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
			return $html;
		}
		//add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
		//add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );
		
	/** Deregister Contact Form 7 CSS */
		function wps_deregister_styles() {
			wp_deregister_style( 'contact-form-7' );
		}
		//add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

/**
 * SHORTCODES
 *
 * 01	Add Code e.g. [code]&lt;?php echo 'Hello World!' ?&gt;[/code]
 *
 */		
		function bbcode( $attr, $content = null ) {
        $content = clean_pre($content); // Clean pre-tags
        return '<pre"><code>' .
                str_replace('<', '<', $content) . // Escape < chars
                '</code></pre>';
	    }
		add_shortcode('code', 'bbcode');

/**
 * FIXES
 * 
 * 01	Sharpen Uploaded Images when Resized
 * 
 */
		
	/** Sharpen Uploaded Images when Resized */
		function ajx_sharpen_resized_files($resized_file) {
			$image = wp_load_image($resized_file);
			if(!is_resource($image))
				return new WP_Error('error_loading_image',$image,$file);
		
				$size = @getimagesize( $resized_file );
				if (!$size)
					return new WP_Error('invalid_image', __('Could not read image size'), $file);
					list($orig_w, $orig_h, $orig_type) = $size;
		
					switch ( $orig_type ) {
						case IMAGETYPE_JPEG:
							$matrix = array(
							array(-1, -1, -1),
							array(-1, 16, -1),
							array(-1, -1, -1),
							);
		
							$divisor = array_sum(array_map('array_sum', $matrix));
							$offset = 0;
							imageconvolution($image, $matrix, $divisor, $offset);
							imagejpeg($image, $resized_file,apply_filters( 'jpeg_quality',90,'edit_image'));
							break;
						case IMAGETYPE_PNG:
							return $resized_file;
						case IMAGETYPE_GIF:
							return $resized_file;
					}
		
					return $resized_file;
		}
		add_filter('image_make_intermediate_size','ajx_sharpen_resized_files',900);
?>