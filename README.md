# README #

## Description ##
WordPress Hardening and Settings Plugin

## License ##
GNU

## How to use ##
Comment or uncomment rows if needed

## Settings overview ##
### DEFINE Wordpress Settings ###
* 01	Define Debug Mode is enabled or disabled to Display PHP Errors
    * 	01.1	Define to load uncompressed CSS- and JavaScript
    * 	01.2	Define to output DB Queries
* 02	Define Wordpress Auto-Updates
* 03	Define Post Revisions is enabled or disabled
    * 	03.1	Define Limit of Post Revisions
* 04	Define Autosave Interval | Value = Seconds | std. = 60
* 05	Define Empty Trash Days | Value = Days | 0 = Disable Trashcan | std. = 30
* 06	Define Editor is enabled or disabled (Theme and Plugin)

### HARDENING Wordpress ###
* 01	Disable XMLRPC
    * 	01.1	Remove XMLRPC-Entry from HTTP-Header

### REMOVE Unnecessary Things ###
* 01	Remove Unnecessary Junk in the Header
* 02	Remove RSS-Feeds
* 03	Remove Header Links
* 04	Remove Version Info from Head and Feeds

### SPEED UP ###
* 01	Add the google CDN version of jQuery for the frontend | Make sure you use this with wp_enqueue_script('jquery'); in header

### CUSTOMIZE Dashboard-Widgets, -Menu and Admin-Bar ###
* 01	Remove Dashboard-Widgets
* 02	Hide Unused Dashboard Menu Items
* 03	Hide Admin-Bar Menu Items

### CUSTOMIZE Backend ###
* 01	Change Loginpage Image
    * 	01.1	Change Loginpage Image URL
    * 	01.2	Namespace Loginpage Image Headertitle
* 02	Add Admin Headerimage in Dashboard
* 03	Add Custom Dashboard Widget
* 04	Userprofil Contactfields
    * 	04.1	Remove unnecessary Contactfields in Userprofil
    * 	04.2	Add Contactfields in Userprofil (e.g. <?php echo $curauth->twitter; ?>)
* 05	Add Custom CSS for the Login Page (Create wp-login.css in your theme folder)
* 06	Editor
    * 	06.1	Add Automatic Media Shortcodes
    * 	06.2	Add Custom CSS to backend Editor 
* 07	Add Post/Page IDs in Admin Table
* 08	Capture users last login date and time
    * 	08.1	Set last login date
    * 	08.2	Get last login date
* 09	Add New Role
    * 	09.1	Remove Role
* 10	Allow SVG through WordPress Media Uploader

### CUSTOMIZE Frontend ###
* 01	Add Custom Post Types for WordPress-Search
* 02	Add Custom excerpt length
* 03	Add Favicon
* 04	Remove Categories from Frontpage
* 05	Remove Width and Height Attributes From Inserted Images
* 06	Deregister Contact Form 7 CSS

### FIXES ###
* 01	Sharpen Uploaded Images when Resized